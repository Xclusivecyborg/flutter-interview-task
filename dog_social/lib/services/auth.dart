import 'package:dio/dio.dart';
import 'package:dog_social/services/exceptions.dart';

class AuthService {
  Dio dio = Dio();
  String endpointUrl = 'https://hookb.in/mZZ8pmBdk6ilzXNNzQGp';

  Future<Map<String, dynamic>?> signUp(
      {String? email, String? password, String? fullname}) async {
    Response response;
    try {
      response = await dio.post(endpointUrl,
          data: {"email": email, "password": password, "fullname": fullname});
      if (response.statusCode == 200) {
        return response.data;
      } else {
        throw {
          Exception(message: 'Unable to login'),
        };
      }
    } on DioError catch (e) {
      if (e.response!.statusCode == 401) {
        print(Exception(message: 'An Unexpected error has occured'));
      } else {
        print(e.response!.statusCode);
      }
    } catch (e) {
      print(e);
    }
  }
}
