class User {
  String? name;
  int? distance;
  int? price;
  String? imageUrl;
  bool? isOnline;
  int? lastSentTime;

  User(
      {this.isOnline = false,
      this.imageUrl,
      this.lastSentTime,
      this.name,
      this.distance,
      this.price});
}
