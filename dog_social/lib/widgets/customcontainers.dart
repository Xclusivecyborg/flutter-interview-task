import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../constants.dart';

Container lineThrough() {
  return Container(
    height: 2,
    width: 10,
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(40),
    ),
  );
}

Container circleText({String? text, Color? textColor, Color? boxColor}) {
  return Container(
    alignment: Alignment.center,
    child: Text(
      text!,
      style: GoogleFonts.poppins(
        color: textColor,
        fontSize: 17,
      ),
    ),
    height: 35,
    width: 35,
    decoration: BoxDecoration(
      color: boxColor,
      borderRadius: BorderRadius.circular(40),
    ),
  );
}

Container onlineStatusDot({bool isOnline = false}) {
  return Container(
    height: 10,
    width: 10,
    decoration: BoxDecoration(
      color: isOnline ? kMylightOrange : Colors.transparent,
      shape: BoxShape.circle,
    ),
  );
}

Container onlineStatusDot2({bool isOnline = false}) {
  return Container(
    height: 10,
    width: 10,
    decoration: BoxDecoration(
      color: isOnline ? kMyGreen : Colors.transparent,
      shape: BoxShape.circle,
    ),
  );
}

Container statusDot() {
  return Container(
    height: 8,
    width: 8,
    decoration: BoxDecoration(
      color: kMyRed,
      shape: BoxShape.circle,
    ),
  );
}
