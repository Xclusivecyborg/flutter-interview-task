import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../constants.dart';

class CustomTextField extends StatelessWidget {
  final IconData? icon;
  final String? hint;
  final String? label;
  final bool? obscure;
  final TextEditingController? controller;
  CustomTextField({
    Key? key,
    this.icon,
    @required this.hint,
    this.obscure = false,
    @required this.label,
    @required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 16, top: 10),
      child: Stack(
        children: [
          Text(
            label!,
            style: GoogleFonts.poppins(
              fontWeight: FontWeight.w500,
              color: kMyGrey,
              fontSize: 11,
            ),
          ),
          TextFormField(
            key: Key('My Textfield '),
            controller: controller,
            obscureText: obscure!,
            decoration: InputDecoration(
              suffixIcon: Icon(
                icon,
                size: 15,
              ),
              hintText: hint!,
              hintStyle: GoogleFonts.poppins(
                fontWeight: FontWeight.w500,
                color: kMyBlack,
                fontSize: 16,
              ),
              border: InputBorder.none,
            ),
          ),
        ],
      ),
      height: 60,
      width: double.infinity,
      decoration: BoxDecoration(
        color: kMylightGrey,
        borderRadius: BorderRadius.circular(10),
      ),
    );
  }
}
