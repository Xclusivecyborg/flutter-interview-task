import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../constants.dart';

class CustomButton extends StatelessWidget {
  final Function()? onpressed;
  final double? height;
  final double? width;
  final String? text;
  CustomButton({
    Key? key,
    @required this.onpressed,
    @required this.text,
    this.height,
    this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextButton(
        key: Key('text button'),
        onPressed: onpressed,
        child: Text(
          text!,
          style: GoogleFonts.poppins(
            fontWeight: FontWeight.w700,
            color: kMyWhite,
            fontSize: 17,
          ),
          textAlign: TextAlign.center,
        ),
      ),
      height: height == null ? 58 : height,
      width: width == null ? double.infinity : width,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [kMydeepOrange!, kMylightOrange!],
        ),
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
    );
  }
}
