import 'package:dog_social/models/usermodel.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../constants.dart';

class CustomListView extends StatelessWidget {
  const CustomListView({
    Key? key,
    required this.userResults,
  }) : super(key: key);

  final List<User> userResults;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      child: ListView.separated(
        itemCount: userResults.length,
        separatorBuilder: (_, index) => SizedBox(
          width: 40,
        ),
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemBuilder: (_, index) => Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 125,
                  width: 179,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage(userResults[index].imageUrl!),
                    ),
                    color: kMyBlack,
                    borderRadius: BorderRadius.circular(15),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        userResults[index].name!,
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w500,
                          color: kMyBlack,
                          fontSize: 17,
                        ),
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.location_on_outlined,
                            size: 15,
                          ),
                          Text(
                            "${userResults[index].distance} km from you",
                            style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w700,
                              color: kMyGrey,
                              fontSize: 10,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Positioned(
              top: 150,
              right: 1,
              child: Container(
                alignment: Alignment.center,
                child: Text(
                  "\$${userResults[index].distance}/h",
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w500,
                    color: kMyWhite,
                    fontSize: 10,
                  ),
                ),
                height: 25,
                width: 48,
                decoration: BoxDecoration(
                  color: kMyBlack,
                  borderRadius: BorderRadius.circular(7),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
