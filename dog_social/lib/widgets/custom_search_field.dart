import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../constants.dart';

class CustomSearchField extends StatelessWidget {
  final IconData? preixIcon;
  final Widget? suffixIcon;
  final String? hint;
  CustomSearchField({
    Key? key,
    @required this.hint,
    this.preixIcon,
    this.suffixIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        textAlign: TextAlign.left,
        decoration: InputDecoration(
          suffixIcon: suffixIcon,
          prefixIcon: Icon(preixIcon),
          hintText: hint!,
          hintStyle: GoogleFonts.poppins(
            fontWeight: FontWeight.w400,
            color: kMyGrey,
            fontSize: 16,
          ),
          border: InputBorder.none,
        ),
      ),
      height: 43.5,
      width: double.infinity,
      decoration: BoxDecoration(
        color: kMylightGrey,
        borderRadius: BorderRadius.circular(10),
      ),
    );
  }
}
