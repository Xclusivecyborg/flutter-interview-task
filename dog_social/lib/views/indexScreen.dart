import 'package:dog_social/views/homescreen/homeScreen.dart';
import 'package:dog_social/views/moments/moments.dart';
import 'package:dog_social/views/profilescreen/profile.dart';
import 'package:dog_social/widgets/customcontainers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../constants.dart';
import 'chatscreen/chatScreen.dart';

class IndexScreen extends StatefulWidget {
  const IndexScreen({Key? key}) : super(key: key);

  @override
  _IndexScreenState createState() => _IndexScreenState();
}

class _IndexScreenState extends State<IndexScreen> {
  List selections = [
    HomeScreen(),
    Moments(),
    ChatScreen(),
    ProfileView(),
  ];

  int selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: selections.elementAt(selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        onTap: (int index) {
          setState(() {
            selectedIndex = index;
          });
        },
        currentIndex: selectedIndex,
        iconSize: 30,
        selectedItemColor: kMyBlack,
        showUnselectedLabels: true,
        unselectedItemColor: kMyGrey,
        items: [
          BottomNavigationBarItem(
            label: 'Home',
            icon: Icon(Icons.home_filled),
          ),
          BottomNavigationBarItem(
            label: 'Moments',
            icon: Icon(FontAwesomeIcons.users),
          ),
          BottomNavigationBarItem(
            label: 'Chat',
            icon: Stack(
              children: [
                Icon(CupertinoIcons.paperplane_fill),
                Padding(
                  padding: const EdgeInsets.only(top: 1.0, left: 14.0),
                  child: statusDot(),
                ),
              ],
            ),
          ),
          BottomNavigationBarItem(
            label: 'Profile',
            icon: Icon(Icons.person),
          ),
        ],
      ),
    );
  }
}
