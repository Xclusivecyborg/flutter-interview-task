import 'package:dog_social/views/chatscreen/chatScreen_view_model.dart';
import 'package:dog_social/views/chatview/chatView.dart';
import 'package:dog_social/widgets/custom_search_field.dart';
import 'package:dog_social/widgets/customcontainers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../constants.dart';

class ChatScreen extends StatefulWidget {
  ChatScreen({Key? key}) : super(key: key);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  ChatScreeViewModel model = ChatScreeViewModel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 17.0, left: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Chat",
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w700,
                      color: kMyBlack,
                      fontSize: 34,
                    ),
                  ),
                  SizedBox(height: 20),
                  CustomSearchField(
                    preixIcon: Icons.search,
                    hint: 'search',
                    suffixIcon: Padding(
                      padding: const EdgeInsets.only(top: 5.0),
                      child: Image.asset(
                        'assets/Filter.png',
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            Container(
              child: ListView.separated(
                shrinkWrap: true,
                separatorBuilder: (_, index) => Divider(
                  height: 10,
                  color: kMyGrey,
                ),
                itemCount: model.userResults.length,
                itemBuilder: (_, index) => ListTile(
                  onTap: () {
                    Navigator.push(
                        context, MaterialPageRoute(builder: (_) => ChatView()));
                  },
                  leading: Container(
                    alignment: Alignment.centerLeft,
                    height: 50,
                    width: 50,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage(model.userResults[index].imageUrl!),
                      ),
                      color: kMyBlack,
                      borderRadius: BorderRadius.circular(30),
                    ),
                  ),
                  title: Text(
                    model.userResults[index].name!,
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w700,
                      color: kMyBlack,
                      fontSize: 17,
                    ),
                  ),
                  subtitle: Row(
                    children: [
                      Text(
                        "Hey! how's your dog?",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w500,
                          color: kMysemiblack,
                          fontSize: 17,
                        ),
                      ),
                      Text(
                        " ${model.userResults[index].lastSentTime}min",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w500,
                          color: kMysemiblack,
                          fontSize: 17,
                        ),
                      ),
                    ],
                  ),
                  trailing: onlineStatusDot(
                      isOnline: model.userResults[index].isOnline!),
                ),
              ),
            ),
          ],
        ),
      )),
    );
  }
}
