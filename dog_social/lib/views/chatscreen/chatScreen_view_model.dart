import 'package:dog_social/models/usermodel.dart';

class ChatScreeViewModel {
  List<User> userResults = [
    User(
        isOnline: true,
        lastSentTime: 1,
        distance: 7,
        name: 'Mason York',
        price: 3,
        imageUrl: 'assets/1.png'),
    User(
        distance: 4,
        lastSentTime: 6,
        name: 'Mark Greene',
        price: 2,
        imageUrl: 'assets/2.png'),
    User(
        isOnline: false,
        lastSentTime: 2,
        distance: 5,
        name: 'Jason Mount',
        price: 10,
        imageUrl: 'assets/3.png'),
    User(
        isOnline: true,
        lastSentTime: 3,
        distance: 10,
        name: 'Glory Pierce',
        price: 8,
        imageUrl: 'assets/4.png'),
    User(
        isOnline: false,
        lastSentTime: 1,
        distance: 12,
        name: 'John Knox',
        price: 4,
        imageUrl: 'assets/5.png'),
    User(
        isOnline: false,
        lastSentTime: 1,
        distance: 8,
        name: 'Drew Gold',
        price: 3,
        imageUrl: 'assets/Profile.png'),
  ];
}
