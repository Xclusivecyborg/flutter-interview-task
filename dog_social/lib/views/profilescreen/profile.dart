import 'package:dog_social/constants.dart';
import 'package:dog_social/widgets/customButton.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.6,
                  width: double.infinity,
                  child: Stack(
                    children: [
                      SafeArea(
                        child: SizedBox(
                          width: double.infinity,
                          height: MediaQuery.of(context).size.height * 0.44,
                          child: Image.asset(
                            'assets/guy.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SafeArea(
                          child: Align(
                        alignment: Alignment.topCenter,
                        child: Padding(
                          padding: EdgeInsets.all(20),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Container(
                                  height: 44,
                                  width: 44,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(22),
                                    color: Color.fromARGB(150, 196, 196, 196),
                                  ),
                                  child: Center(
                                    child: Icon(
                                      Icons.close,
                                      color: kMyWhite,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                height: 44,
                                width: 101,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(22),
                                    color: Color.fromARGB(150, 196, 196, 196)),
                                child: Center(
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text(
                                        "verified",
                                        style: TextStyle(
                                            color: kMyWhite,
                                            fontSize: 13,
                                            fontWeight: FontWeight.w700),
                                      ),
                                      Icon(
                                        Icons.check_box,
                                        color: kMyWhite,
                                      )
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ))
                    ],
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.4,
                  child: Container(
                    color: kMyWhite,
                  ),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              padding:
                  EdgeInsets.only(left: 10, right: 10, bottom: 10, top: 20),
              height: MediaQuery.of(context).size.height * 0.57,
              width: double.infinity,
              decoration: BoxDecoration(
                color: kMyWhite,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(24),
                  topLeft: Radius.circular(24),
                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Alex Murray',
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w700, fontSize: 28),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Text.rich(
                    TextSpan(
                      text: "5\$",
                      style: GoogleFonts.poppins(
                          color: kMyBlack,
                          fontWeight: FontWeight.w500,
                          fontSize: 13),
                      children: [
                        TextSpan(
                          text: "/hr | ",
                          style: GoogleFonts.poppins(
                              color: kMyGrey,
                              fontWeight: FontWeight.w500,
                              fontSize: 13),
                        ),
                        TextSpan(
                          text: "10",
                          style: GoogleFonts.poppins(
                              color: kMyBlack,
                              fontWeight: FontWeight.w500,
                              fontSize: 13),
                        ),
                        TextSpan(
                          text: "km |  ",
                          style: GoogleFonts.poppins(
                              color: kMyGrey,
                              fontWeight: FontWeight.w500,
                              fontSize: 13),
                        ),
                        TextSpan(
                          text: "4.4  ",
                          style: GoogleFonts.poppins(
                              color: kMyBlack,
                              fontWeight: FontWeight.w500,
                              fontSize: 13),
                        ),
                        TextSpan(
                          text: "|  ",
                          style: GoogleFonts.poppins(
                              color: kMyGrey,
                              fontWeight: FontWeight.w500,
                              fontSize: 13),
                        ),
                        TextSpan(
                          text: "450 ",
                          style: GoogleFonts.poppins(
                              color: kMyBlack,
                              fontWeight: FontWeight.w500,
                              fontSize: 13),
                        ),
                        TextSpan(
                          text: "walks",
                          style: GoogleFonts.poppins(
                              color: kMyGrey,
                              fontWeight: FontWeight.w500,
                              fontSize: 13),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Divider(
                    color: kMyGrey,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 54,
                        width: 109,
                        decoration: BoxDecoration(
                            color: Color(0xff2B2B2B),
                            borderRadius: BorderRadius.circular(14)),
                        child: Center(
                          child: Text(
                            "About",
                            style: GoogleFonts.poppins(
                                color: kMyWhite,
                                fontWeight: FontWeight.w700,
                                fontSize: 13),
                          ),
                        ),
                      ),
                      Container(
                        height: 54,
                        width: 109,
                        decoration: BoxDecoration(
                            color: kMylightGrey,
                            borderRadius: BorderRadius.circular(14)),
                        child: Center(
                          child: Text(
                            "Location",
                            style: GoogleFonts.poppins(
                                color: kMyGrey,
                                fontWeight: FontWeight.w700,
                                fontSize: 13),
                          ),
                        ),
                      ),
                      Container(
                        height: 54,
                        width: 109,
                        decoration: BoxDecoration(
                            color: kMylightGrey,
                            borderRadius: BorderRadius.circular(14)),
                        child: Center(
                          child: Text(
                            "Reviews",
                            style: GoogleFonts.poppins(
                                color: kMyGrey,
                                fontWeight: FontWeight.w700,
                                fontSize: 13),
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 18,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Age",
                            style: GoogleFonts.poppins(
                                color: kMyGrey,
                                fontWeight: FontWeight.w500,
                                fontSize: 13),
                          ),
                          Text(
                            "30 years",
                            style: GoogleFonts.poppins(
                                color: kMyBlack,
                                fontWeight: FontWeight.w500,
                                fontSize: 17),
                          )
                        ],
                      ),
                      SizedBox(
                        width: 45,
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Experience",
                            style: GoogleFonts.poppins(
                                color: kMyGrey,
                                fontWeight: FontWeight.w500,
                                fontSize: 13),
                          ),
                          Text(
                            "11 months",
                            style: GoogleFonts.poppins(
                                color: kMyBlack,
                                fontWeight: FontWeight.w500,
                                fontSize: 17),
                          )
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: SizedBox(
                      width: 300,
                      child: Text(
                        "Alex has loved dogs since childhood. He is currently a veterinary student. Visits the \ndog shelter we...",
                        style: GoogleFonts.poppins(
                            color: kMyGrey,
                            fontWeight: FontWeight.w500,
                            fontSize: 13),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: SizedBox(
                      width: 300,
                      child: Text(
                        "Read more",
                        style: GoogleFonts.poppins(
                            color: kMylightOrange,
                            fontWeight: FontWeight.w500,
                            fontSize: 13),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  CustomButton(onpressed: () {}, text: 'Check Schedule'),
                  SizedBox(
                    height: 37,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class ProfileView extends StatelessWidget {
  const ProfileView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('Profile view'),
      ),
    );
  }
}
