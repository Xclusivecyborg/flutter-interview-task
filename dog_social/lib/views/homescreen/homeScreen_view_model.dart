import 'package:dog_social/models/usermodel.dart';

class HomeScreenViewModel {
  List<User> userResults = [
    User(
        distance: 7,
        name: 'Mason York',
        price: 3,
        imageUrl: 'assets/Profile.png'),
    User(
        distance: 4,
        name: 'Mark Greene',
        price: 2,
        imageUrl: 'assets/persondog.png'),
    User(
        distance: 5,
        name: 'Jason Mount',
        price: 10,
        imageUrl: 'assets/Profile.png'),
    User(
        distance: 10,
        name: 'Glory Pierce',
        price: 8,
        imageUrl: 'assets/Profile.png'),
    User(
        distance: 12,
        name: 'John Knox',
        price: 4,
        imageUrl: 'assets/Profile.png'),
    User(
        distance: 8,
        name: 'Drew Gold',
        price: 3,
        imageUrl: 'assets/Profile.png'),
  ];
}
