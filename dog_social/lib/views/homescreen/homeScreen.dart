import 'package:dog_social/views/homescreen/homeScreen_view_model.dart';
import 'package:dog_social/widgets/custom_listview.dart';
import 'package:dog_social/widgets/custom_search_field.dart';
import 'package:dog_social/widgets/customcontainers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../constants.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  HomeScreenViewModel model = HomeScreenViewModel();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kMyWhite,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 16.0, right: 16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Home",
                            style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w700,
                              color: kMyBlack,
                              fontSize: 34,
                            ),
                          ),
                          Text(
                            "Explore dog walkers",
                            style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w500,
                              color: kMyGrey,
                              fontSize: 17,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    Container(
                      child: TextButton(
                          onPressed: () {},
                          child: Row(
                            children: [
                              Icon(
                                Icons.add,
                                color: kMyWhite,
                              ),
                              Text(
                                'Book a walk',
                                style: GoogleFonts.poppins(
                                  fontWeight: FontWeight.w700,
                                  color: kMyWhite,
                                  fontSize: 10,
                                ),
                                textAlign: TextAlign.center,
                              )
                            ],
                          )),
                      height: 41,
                      width: 104,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          colors: [kMydeepOrange!, kMylightOrange!],
                        ),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 21,
                ),
                CustomSearchField(
                  preixIcon: Icons.location_on_outlined,
                  hint: 'Kyiv, Ukraine',
                  suffixIcon: Stack(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 12.0, left: 10.0),
                        child: statusDot(),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: Image.asset(
                          'assets/Filter.png',
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 21,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Near you",
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w700,
                        color: kMyBlack,
                        fontSize: 34,
                      ),
                    ),
                    Text(
                      "View all",
                      style: GoogleFonts.poppins(
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.w400,
                        color: kMyBlack,
                        fontSize: 17,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                CustomListView(userResults: model.userResults),
                Divider(
                  height: 10,
                  color: kMyGrey,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Suggested",
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w700,
                        color: kMyBlack,
                        fontSize: 34,
                      ),
                    ),
                    Text(
                      "View all",
                      style: GoogleFonts.poppins(
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.w400,
                        color: kMyBlack,
                        fontSize: 17,
                      ),
                    ),
                  ],
                ),
                CustomListView(userResults: model.userResults),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
