import 'package:dog_social/views/signup/signUp.dart';
import 'package:dog_social/widgets/customButton.dart';
import 'package:dog_social/widgets/customcontainers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../constants.dart';

class OnboardingPage extends StatelessWidget {
  const OnboardingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff202020),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  height: 550,
                  width: 500,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage(
                        'assets/onboarding.png',
                      ),
                    ),
                  ),
                  // width: double.infinity,
                ),
                Positioned(
                  bottom: 30,
                  left: 120,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      circleText(
                        boxColor: kMyWhite,
                        textColor: kMyBlack,
                        text: '1',
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      lineThrough(),
                      SizedBox(
                        width: 5,
                      ),
                      circleText(
                        boxColor: kMylightBlack,
                        textColor: kMyWhite,
                        text: '2',
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      lineThrough(),
                      SizedBox(
                        width: 5,
                      ),
                      circleText(
                        boxColor: kMylightBlack,
                        textColor: kMyWhite,
                        text: '3',
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Column(
              children: [
                Text(
                  "Too tired to walk your dog? \nLet's help you! ",
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w700,
                    color: kMyWhite,
                    fontSize: 22,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 15,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 21, right: 30),
                  child: CustomButton(
                    text: "Join our community ",
                    onpressed: () {
                      Navigator.push(
                          context, MaterialPageRoute(builder: (_) => SignUp()));
                    },
                  ),
                ),
                SizedBox(
                  height: 22,
                ),
                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Text(
                    "Already a member?",
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w700,
                      color: kMyWhite,
                      fontSize: 15,
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  GestureDetector(
                    child: Text(
                      "Sign in",
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w700,
                        color: kMydeepOrange,
                        fontSize: 15,
                      ),
                    ),
                  ),
                ])
              ],
            ),
          ],
        ),
      ),
    );
  }
}
