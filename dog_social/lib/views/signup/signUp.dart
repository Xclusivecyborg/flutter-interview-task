import 'package:dog_social/services/auth.dart';
import 'package:dog_social/views/indexScreen.dart';
import 'package:dog_social/widgets/customButton.dart';
import 'package:dog_social/widgets/custom_textfield.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../constants.dart';

class SignUp extends StatefulWidget {
  SignUp({Key? key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  AuthService _authService = new AuthService();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController fullnameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kMyWhite,
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back, color: kMyBlack),
        ),
        backgroundColor: kMyWhite,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 14,
            right: 18,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Let's start here",
                style: GoogleFonts.poppins(
                  fontWeight: FontWeight.w700,
                  color: kMyBlack,
                  fontSize: 34,
                ),
              ),
              Text(
                "Fill in your details to begin",
                style: GoogleFonts.poppins(
                  fontWeight: FontWeight.w500,
                  color: kMyGrey,
                  fontSize: 17,
                ),
              ),
              SizedBox(
                height: 22,
              ),
              CustomTextField(
                controller: fullnameController,
                hint: 'Cristian',
                label: 'Fullname',
              ),
              SizedBox(
                height: 22,
              ),
              CustomTextField(
                controller: emailController,
                hint: 'Cristiandeb13@gmail.com',
                label: 'E -mail',
              ),
              SizedBox(
                height: 22,
              ),
              CustomTextField(
                controller: passwordController,
                hint: '**********',
                obscure: true,
                label: 'Password',
                icon: FontAwesomeIcons.eyeSlash,
              ),
              SizedBox(
                height: 22,
              ),
              CustomButton(
                onpressed: () async {
                  // await _authService.signUp(
                  //     email: emailController.text,
                  //     password: passwordController.text,
                  //     fullname: fullnameController.text);
                  Navigator.push(context,
                      MaterialPageRoute(builder: (_) => IndexScreen()));
                },
                text: 'Sign up',
              ),
              SizedBox(height: 200),
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "By signing in, I agree with",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w500,
                          color: kMyGrey,
                          fontSize: 13,
                        ),
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      Text(
                        "Terms of use",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w500,
                          color: kMyBlack,
                          fontSize: 13,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "and",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w500,
                          color: kMyGrey,
                          fontSize: 13,
                        ),
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      Text(
                        "Privacy Policy",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w500,
                          color: kMyBlack,
                          fontSize: 13,
                        ),
                      ),
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
