import 'package:dog_social/constants.dart';
import 'package:dog_social/views/profilescreen/profile.dart';
import 'package:dog_social/widgets/custom_search_field.dart';
import 'package:dog_social/widgets/customcontainers.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class ChatView extends StatefulWidget {
  const ChatView({Key? key}) : super(key: key);

  @override
  _ChatViewState createState() => _ChatViewState();
}

class _ChatViewState extends State<ChatView> {
  bool? isChatView;

  List messageWidgetList = [
    "Hey, Alex! Nice to meet you! \nI’d like to hire a walker and \nyou’re perfect one for me. \nCan you help me out?",
    "Hi! That’s great! Let me give \nyou a call and we’ll discuss all \nthe details",
    "Okay. I'm waiting for a call"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kMyWhite,
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              FontAwesomeIcons.phoneAlt,
              color: kMyBlack,
              size: 20,
            ),
          ),
        ],
        elevation: 0,
        backgroundColor: kMyWhite,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            FontAwesomeIcons.arrowLeft,
            color: kMyBlack,
            size: 20,
          ),
        ),
        title: Row(
          children: [
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Profile()));
              },
              child: Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/Profile.png'),
                  ),
                  borderRadius: BorderRadius.circular(30),
                ),
              ),
            ),
            SizedBox(
              width: 15,
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Alex Murray',
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w700,
                      color: kMyBlack,
                      fontSize: 17,
                    ),
                  ),
                  Row(
                    children: [
                      onlineStatusDot2(isOnline: true),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        "Online",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w500,
                          color: kMysemiblack,
                          fontSize: 17,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      body: SafeArea(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ListView(
            shrinkWrap: true,
            children: [
              MessageTextWidget(
                messageBody: messageWidgetList[0],
                isMe: true,
              ),
              MessageTextWidget(
                messageBody: messageWidgetList[1],
                isMe: false,
              ),
              MessageTextWidget(
                messageBody: messageWidgetList[2],
                isMe: true,
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(17, 5, 17, 17),
            child: Row(
              children: [
                Container(
                  height: 44,
                  width: 44,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: kMylightGrey,
                  ),
                  child: Icon(Icons.add, color: kMylightOrange),
                ),
                SizedBox(
                  width: 17,
                ),
                Expanded(
                  child: CustomSearchField(
                    hint: 'Aa',
                    suffixIcon: Icon(Icons.mic, color: kMyBlack),
                  ),
                ),
              ],
            ),
          ),
        ],
      )),
    );
  }
}

class MessageTextWidget extends StatelessWidget {
  MessageTextWidget(
      {Key? key, @required this.messageBody, this.isMe, this.timeCreated})
      : super(key: key);

  final messageBody;

  final bool? isMe;
  final timeCreated;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment:
          isMe! ? CrossAxisAlignment.end : CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.all(20),
          padding: EdgeInsets.all(12),
          decoration: BoxDecoration(
            color: isMe! ? kMydeepOrange : kMylightGrey,
            borderRadius: isMe!
                ? BorderRadius.only(
                    topLeft: Radius.circular(
                      20,
                    ),
                    bottomLeft: Radius.circular(20),
                    topRight: Radius.circular(20))
                : BorderRadius.only(
                    topRight: Radius.circular(
                      20,
                    ),
                    topLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20)),
          ),
          child: Text(
            '$messageBody',
            softWrap: true,
            style: GoogleFonts.poppins(
              color: isMe! ? kMyWhite : kMyBlack,
              fontSize: 16.0,
            ),
          ),
        ),
      ],
    );
  }
}
