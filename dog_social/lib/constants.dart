import 'package:flutter/material.dart';

Color? kMyWhite = Colors.white;
Color? kMyBlack = Colors.black;
Color? kMylightBlack = Color(0XFF404040);
Color? kMysemiblack = Color(0XFF4f4f4f);
Color? kMyGrey = Color(0XFF7A7A7A);
Color? kMydeepOrange = Color(0xfffB724C);
Color? kMylightOrange = Color(0xffFE904B);
Color? kMylightGrey = Color(0xFFF0F0F0);
Color? kMyGreen = Color(0xff5AD439);
Color? kMyRed = Color(0xFFE73A40);
